﻿#pragma checksum "..\..\AddUpdateBookDialog.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "5AB91EC9212D07A344FA5924E173A30E45002DF8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using LibraryProject;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace LibraryProject {
    
    
    /// <summary>
    /// AddUpdateBookDialog
    /// </summary>
    public partial class AddUpdateBookDialog : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvBooks;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbTitle;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbISBN;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbAuthor;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbDescription;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbSubject;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbYearPublished;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNumberOfCopies;
        
        #line default
        #line hidden
        
        
        #line 41 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbNumberOfAvailableCopies;
        
        #line default
        #line hidden
        
        
        #line 42 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox tbBookId;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btAddUpdate;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\AddUpdateBookDialog.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btDeleteBook;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/LibraryProject;component/addupdatebookdialog.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\AddUpdateBookDialog.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.lvBooks = ((System.Windows.Controls.ListView)(target));
            
            #line 10 "..\..\AddUpdateBookDialog.xaml"
            this.lvBooks.AddHandler(System.Windows.Controls.Primitives.ButtonBase.ClickEvent, new System.Windows.RoutedEventHandler(this.LvBooks_Click));
            
            #line default
            #line hidden
            
            #line 10 "..\..\AddUpdateBookDialog.xaml"
            this.lvBooks.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.LvBooks_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tbTitle = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.tbISBN = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.tbAuthor = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.tbDescription = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.tbSubject = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.tbYearPublished = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.tbNumberOfCopies = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.tbNumberOfAvailableCopies = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.tbBookId = ((System.Windows.Controls.TextBox)(target));
            return;
            case 11:
            this.btAddUpdate = ((System.Windows.Controls.Button)(target));
            
            #line 44 "..\..\AddUpdateBookDialog.xaml"
            this.btAddUpdate.Click += new System.Windows.RoutedEventHandler(this.AddUpdate_ButtonClick);
            
            #line default
            #line hidden
            return;
            case 12:
            this.btDeleteBook = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\AddUpdateBookDialog.xaml"
            this.btDeleteBook.Click += new System.Windows.RoutedEventHandler(this.BtDeleteBook_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

