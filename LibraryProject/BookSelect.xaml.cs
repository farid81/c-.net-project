﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for LoanWizard.xaml
    /// </summary>
    public partial class BookSelectDialog : Window
    {
        List<Book> BooksList = new List<Book>();

        public BookSelectDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            lvBooks.ItemsSource = BooksList;

            try
            {
                Globals.Db = new Database();
                
                ReloadBookList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadBookList()
        {
            try
            {
                List<Book> list = Globals.Db.GetAllBooks();
                BooksList.Clear();
                foreach (Book b in list)
                {
                    BooksList.Add(b);
                }
                lvBooks.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "Book Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SortByTitle_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void SortBySubject_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void WizardNext_Click(object sender, RoutedEventArgs e)
        {
            Book currBook = lvBooks.SelectedItem as Book;
            if (currBook == null)
            {
                MessageBox.Show("Warning:\n", "You should choose a Book", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else
            {

                Globals.bookSelected = currBook;
                MemberSelectDialog memberSelectDialog = new MemberSelectDialog(this);
                if(memberSelectDialog.ShowDialog() == true)
                {
                    DialogResult = true;
                    return;
                }
            }


        }
    }
}
