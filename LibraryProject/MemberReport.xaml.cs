﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for MemberReport.xaml
    /// </summary>
    public partial class MemberReport : Window
    {
        List<Member> allMembers = new List<Member>();
        public MemberReport(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            try
            {
                Globals.Db = new Database();
                ReloadMemberComboList();
            }
            catch(SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
            "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }


        private void ReloadMemberComboList()
        {

            try
            {
                allMembers = Globals.Db.GetAllMembers();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            foreach (Member m in allMembers)
            {
                cmbMemberReports.Items.Add(m.FamilyName);
            }
        }

        private void CmbMemberReports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedMemberId = allMembers[cmbMemberReports.SelectedIndex].MemberId;
            List<Loan> currentLoans = Globals.Db.GetAllLoansReport(selectedMemberId);
            lvLoans.ItemsSource = currentLoans;

        }

        private void Print_ButtonClick(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();

            if (printDialog.ShowDialog() == true)
            {
                printDialog.PrintVisual(lvLoans, "Report");
            }
        }
    }
}
