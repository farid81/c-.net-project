﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LibraryProject
{
    public class Database
    {
        const string DbConnectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\fesla\Documents\DotnetProject\LibraryProject\LibraryDB.mdf;Integrated Security=True;Connect Timeout=30";

        private SqlConnection conn;

        public Database()
        {
            conn = new SqlConnection(DbConnectionString);
            conn.Open();
        }

        public List<Book> GetAllBooks()
        {
            List<Book> list = new List<Book>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Books", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["BookId"];
                    string title = (string)reader["Title"];
                    string isbn = (string)reader["ISBN"];
                    string author = (string)reader["Author"];
                    string description = (string)reader["Description"];
                    string subject = (string)reader["Subject"];
                    int yearPublished = (int)reader["YearPublished"];
                    int numberOfCopies = (int)reader["NumberOfCopies"];
                    int numberOfAvailableCopies = (int)reader["NumberOfAvailableCopies"];
                    list.Add(new Book() { BookId = id, Title = title, ISBN = isbn, Author = author, Description = description, Subject = subject, YearPublished = yearPublished, NumberOfCopies = numberOfCopies, NumberOfAvailableCopies = numberOfAvailableCopies });
                }
            }
            return list;
        }

        public List<Member> GetAllMembers()
        {
            List<Member> list = new List<Member>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Members", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader["MemberId"];
                    string name = (string)reader["Name"];
                    string familyName = (string)reader["FamilyName"];
                    int birthDate = (int)reader["BirthDate"];
                    string address = (string)reader["Address"];
                    string postalCode = (string)reader["PostalCode"];
                    string cityName = (string)reader["CityName"];
                    string email = (string)reader["Email"];
                    string phoneNumber = (string)reader["PhoneNumber"];

                    list.Add(new Member() { MemberId = id, Name = name, FamilyName = familyName, BirthDate = birthDate, Address = address, PostalCode = postalCode, CityName = cityName, Email = email, PhoneNumber = phoneNumber});
                }
            }
            return list;
        }

        public void ReturnABook()
        {
            SqlCommand updateLoanCmd = new SqlCommand("UPDATE Loans SET DateReturned = @DateReturned WHERE LoanId = @LoanId", conn);
            updateLoanCmd.Parameters.AddWithValue("DateReturned", DateTime.Now);
            updateLoanCmd.Parameters.AddWithValue("LoanId", Globals.selecteLoan.LoanId);
            updateLoanCmd.ExecuteNonQuery();
            int availableCopies = NumberOfAvailableCopies(Globals.selecteLoan.BookId);
            UpdateAvailableCopies(Globals.selecteLoan.BookId, availableCopies+1);
        }

        public List<Loan> GetAllLoans()
        {
            List<Loan> list = new List<Loan>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Loans WHERE DateReturned IS null", conn);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int loanId = (int)reader["LoanId"];
                    DateTime dateIssued = (DateTime)reader["DateIssued"];
                    
                    int bookId = (int)reader["BookId"];
                    int memberId = (int)reader["MemberId"];
                    list.Add(new Loan() {LoanId=loanId, DateIssued = dateIssued, BookId = bookId, MemberId = memberId});
                }
            }
            return list;
        }

        public List<Loan> GetAllLoans(int bookId)
        {
            List<Loan> list = new List<Loan>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Loans WHERE BookId = @BookId", conn);
            cmdSelect.Parameters.AddWithValue("BookId", bookId);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int loanId = (int)reader["LoanId"];
                    DateTime dateIssued = (DateTime)reader["DateIssued"];
                    DateTime? dateReturned;
                    if(reader["DateReturned"].ToString() == "")
                    {
                        dateReturned = null;
                    }
                    else
                    {
                        dateReturned = (DateTime)reader["DateReturned"];
                    }
                    int memberId = (int)reader["MemberId"];
                    list.Add(new Loan() { LoanId = loanId, DateIssued = dateIssued, DateReturned = dateReturned, BookId = bookId, MemberId = memberId });
                }
            }
            return list;
        }


        public List<Loan> GetAllLoansReport(int memberId)
        {
            List<Loan> list = new List<Loan>();
            SqlCommand cmdSelect = new SqlCommand("SELECT * FROM Loans WHERE MemberId = @MemberId", conn);
            cmdSelect.Parameters.AddWithValue("MemberId", memberId);
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    int loanId = (int)reader["LoanId"];
                    DateTime dateIssued = (DateTime)reader["DateIssued"];
                    DateTime? dateReturned;
                    if (reader["DateReturned"].ToString() == "")
                    {
                        dateReturned = null;
                    }
                    else
                    {
                        dateReturned = (DateTime)reader["DateReturned"];
                    }
                    int bookId = (int)reader["BookId"];
                    list.Add(new Loan() { LoanId = loanId, DateIssued = dateIssued, DateReturned = dateReturned, BookId = bookId, MemberId = memberId });
                }
            }
            return list;
        }

        public int NumberOfAvailableCopies(int bookId)
        {
            SqlCommand cmdSelect = new SqlCommand("SELECT NumberOfAvailableCopies from Books WHERE BookId = @BookId", conn);
            cmdSelect.Parameters.AddWithValue("BookId", bookId);
            int availableCopies = 0;
            using (SqlDataReader reader = cmdSelect.ExecuteReader())
            {
                while (reader.Read())
                {
                    availableCopies = (int)reader["NumberOfAvailableCopies"];
                }
            }
            return availableCopies;
        }

        public void LoanABook(int availableCopies)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Loans (DateIssued, BookId, MemberId) VALUES (@DateIssued, @BookId, @MemberId)", conn);
            cmdInsert.Parameters.AddWithValue("DateIssued",DateTime.Today);
            cmdInsert.Parameters.AddWithValue("BookId",Globals.bookSelected.BookId);
            cmdInsert.Parameters.AddWithValue("MemberId",Globals.memberSelected.MemberId);
            cmdInsert.ExecuteNonQuery();
            UpdateAvailableCopies(Globals.bookSelected.BookId,availableCopies - 1);
        }

        public void UpdateAvailableCopies(int bookId, int availableCopies)
        {
            SqlCommand cmdUpdateBook = new SqlCommand("UPDATE Books SET NumberOfAvailableCopies = @NumberofAvailableCopies WHERE BookId = @BookId", conn);
            cmdUpdateBook.Parameters.AddWithValue("NumberOfAvailableCopies", availableCopies);
            cmdUpdateBook.Parameters.AddWithValue("BookId", bookId);
            cmdUpdateBook.ExecuteNonQuery();
        }
        public int AddBook(Book book)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Books (Title, ISBN , Author, Description, Subject, YearPublished, NumberOfCopies,NumberOfAvailableCopies) OUTPUT INSERTED.BookId VALUES (@Title, @ISBN , @Author, @Description, @Subject, @YearPublished, @NumberOfCopies, @NumberOfAvailableCopies)", conn);
            cmdInsert.Parameters.AddWithValue("Title", book.Title);
            cmdInsert.Parameters.AddWithValue("ISBN", book.ISBN);
            cmdInsert.Parameters.AddWithValue("Author", book.Author);
            cmdInsert.Parameters.AddWithValue("Description", book.Description);
            cmdInsert.Parameters.AddWithValue("Subject", book.Subject);
            cmdInsert.Parameters.AddWithValue("YearPublished", book.YearPublished);
            cmdInsert.Parameters.AddWithValue("NumberOfCopies", book.NumberOfCopies);
            cmdInsert.Parameters.AddWithValue("NumberOfAvailableCopies", book.NumberOfAvailableCopies);
            int insertId = (int)cmdInsert.ExecuteScalar();
            return insertId;
        }

        public bool UpdateBook(Book book)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE Books SET Title = @Title, ISBN = @ISBN, Author = @Author, Description = @Description, Subject = @Subject, YearPublished = @YearPublished, NumberOfCopies= @NumberOfCopies, NumberOfAvailableCopies = @NumberOfAvailableCopies WHERE BookId = @BookId" , conn);
            cmdUpdate.Parameters.AddWithValue("Title", book.Title);
            cmdUpdate.Parameters.AddWithValue("ISBN", book.ISBN);
            cmdUpdate.Parameters.AddWithValue("Author", book.Author);
            cmdUpdate.Parameters.AddWithValue("Description", book.Description);
            cmdUpdate.Parameters.AddWithValue("Subject", book.Subject);
            cmdUpdate.Parameters.AddWithValue("YearPublished", book.YearPublished);
            cmdUpdate.Parameters.AddWithValue("NumberOfCopies", book.NumberOfCopies);
            cmdUpdate.Parameters.AddWithValue("NumberOfAvailableCopies", book.NumberOfAvailableCopies);
            cmdUpdate.Parameters.AddWithValue("BookId", book.BookId);
            int affectedRows = cmdUpdate.ExecuteNonQuery();
            return affectedRows > 0;
        }

        public bool DeleteBook(int BookId)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Books WHERE BookId = @BookId", conn);
            cmdDelete.Parameters.AddWithValue("BookId", BookId);
            //cmdDelete.ExecuteNonQuery();
            int affectedRows = cmdDelete.ExecuteNonQuery();
            return affectedRows > 0;
           
        }

        public int AddMember(Member member)
        {
            SqlCommand cmdInsert = new SqlCommand("INSERT INTO Members (Name, FamilyName , BirthDate, Address, PostalCode, CityName, Email, PhoneNumber) OUTPUT INSERTED.MemberId VALUES (@Name, @FamilyName , @BirthDate, @Address, @PostalCode, @CityName, @Email, @PhoneNumber)", conn);
            cmdInsert.Parameters.AddWithValue("Name", member.Name);
            cmdInsert.Parameters.AddWithValue("FamilyName", member.FamilyName);
            cmdInsert.Parameters.AddWithValue("BirthDate", member.BirthDate);
            cmdInsert.Parameters.AddWithValue("Address", member.Address);
            cmdInsert.Parameters.AddWithValue("PostalCode", member.PostalCode);
            cmdInsert.Parameters.AddWithValue("CityName", member.CityName);
            cmdInsert.Parameters.AddWithValue("Email", member.Email);
            cmdInsert.Parameters.AddWithValue("PhoneNumber", member.PhoneNumber);
            int insertId = (int)cmdInsert.ExecuteScalar();
            //member.MemberId = insertId; 
            return insertId;
        }

        public bool UpdateMember(Member member)
        {
            SqlCommand cmdUpdate = new SqlCommand("UPDATE Members SET Name = @Name, FamilyName = @FamilyName, BirthDate = @BirthDate, Address = @Address, PostalCode = @PostalCode, CityName = @CityName, Email = @Email, PhoneNumber = @PhoneNumber WHERE MemberId = @MemberId", conn);
            cmdUpdate.Parameters.AddWithValue("Name", member.Name);
            cmdUpdate.Parameters.AddWithValue("FamilyName", member.FamilyName);
            cmdUpdate.Parameters.AddWithValue("BirthDate", member.BirthDate);
            cmdUpdate.Parameters.AddWithValue("Address", member.Address);
            cmdUpdate.Parameters.AddWithValue("PostalCode", member.PostalCode);
            cmdUpdate.Parameters.AddWithValue("CityName", member.CityName);
            cmdUpdate.Parameters.AddWithValue("Email", member.Email);
            cmdUpdate.Parameters.AddWithValue("PhoneNumber", member.PhoneNumber);
            cmdUpdate.Parameters.AddWithValue("MemberId", member.MemberId);
            int affectedRows = cmdUpdate.ExecuteNonQuery();
            return affectedRows > 0;
        }


        public bool DeleteMember(int MemberId)
        {
            SqlCommand cmdDelete = new SqlCommand("DELETE FROM Members WHERE MemberId = @MemberId", conn);
            cmdDelete.Parameters.AddWithValue("MemberId", MemberId);
            int affectedRows = cmdDelete.ExecuteNonQuery();
            return affectedRows > 0;
        }
    }
}
