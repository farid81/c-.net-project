﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{
    public class Loan
    {
        public int LoanId { get; set; }
        public DateTime DateIssued { get; set; }
        public DateTime? DateReturned { get; set; }
        public int BookId { get; set; }
        public int MemberId { get; set; }

    }
}
