﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for MemberSelectDialog.xaml
    /// </summary>
    public partial class MemberSelectDialog : Window
    {
        List<Member> MembersList = new List<Member>();

        public MemberSelectDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            lvMembers.ItemsSource = MembersList;

            try
            {
                Globals.Db = new Database();

                ReloadMemberList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadMemberList()
        {
            try
            {
                List<Member> list = Globals.Db.GetAllMembers();
                MembersList.Clear();
                foreach (Member m in list)
                {
                    MembersList.Add(m);
                }
                lvMembers.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void SortByName_MenuClick(object sender, RoutedEventArgs e)
        {

        }

        private void WizardNext_Click(object sender, RoutedEventArgs e)
        {
            Member currMember = lvMembers.SelectedItem as Member;
            if (currMember == null)
            {
                MessageBox.Show("You should choose a Member:\n", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (currMember != null)
            {
               Globals.memberSelected = currMember;
                MessageBoxResult result = MessageBox.Show("Are you sure you want to issue the book " + Globals.bookSelected.Title + " to the member " + Globals.memberSelected.Name + " " + Globals.memberSelected.FamilyName + "?", "Loan Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (result == MessageBoxResult.No)
                    return;
                else
                {

                    try
                    {
                        int availableCopies = Globals.Db.NumberOfAvailableCopies(Globals.bookSelected.BookId);

                        if (availableCopies <= 0)
                        {
                            MessageBox.Show("The book is not available. All copies of this books are loaned.");
                            DialogResult = true;
                            return;
                        }

                        Globals.Db.LoanABook(availableCopies);
                        MessageBox.Show("The loan is successful");
                        DialogResult = true;
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("Database Error" + Environment.NewLine + ex);
                    }
                }

            }
        }
    }
}
