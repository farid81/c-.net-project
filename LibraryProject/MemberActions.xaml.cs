﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for MemberActions.xaml
    /// </summary>
    public partial class MemberActions : Window
    {
        Member selectedMember;
        List<Member> MembersList = new List<Member>();

        public MemberActions()
        {
            InitializeComponent();
            lvMembers.ItemsSource = MembersList;


            try
            {
                Globals.Db = new Database();

                ReloadMemberList();


            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }


        private void ReloadMemberList()
        {
            try
            {
                List<Member> list = Globals.Db.GetAllMembers();
                MembersList.Clear();
                foreach (Member m in list)
                {
                    MembersList.Add(m);
                }
                lvMembers.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("There is no book in the database yet" + ex);
            }
        }

        private void ResetForm()
        {
            tbMemberId.Text = "";
            tbName.Text = "";
            tbFamilyName.Text = "";
            tbBirthDate.Text = "";
            tbAddress.Text = "";
            tbPostalCode.Text = "";
            tbCityName.Text = "";
            tbEmail.Text = "";
            tbPhoneNumber.Text = "";
            btDeleteMember.IsEnabled = false;
        }

        private string ValidateForm()
        {
            string validationMessage = "";
            if (tbName.Text == "")
                validationMessage += "Name can't be empty" + Environment.NewLine;
            if (tbFamilyName.Text == "")
                validationMessage += "FamilyName can't be empty" + Environment.NewLine;
            if (tbBirthDate.Text == "")
                validationMessage += "BirthDate can't be empty" + Environment.NewLine;
            if (tbAddress.Text == "")
                validationMessage += "Address can't be empty" + Environment.NewLine;
            if (tbPostalCode.Text == "")
                validationMessage += "PostalCode can't be empty" + Environment.NewLine;
            if (tbCityName.Text == "")
                validationMessage += "CityName can't be empty" + Environment.NewLine;
            if (tbEmail.Text == "")
                validationMessage += "Email can't be empty" + Environment.NewLine;
            if (tbPhoneNumber.Text == "")
                validationMessage += "PhoneNumber can't be empty" + Environment.NewLine;
        
            return validationMessage;
        }



        private void LvMembers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvMembers.SelectedItem != null)
            {
                selectedMember = lvMembers.SelectedItem as Member;

                tbMemberId.Text = selectedMember.MemberId.ToString();
                tbName.Text = selectedMember.Name;
                tbFamilyName.Text = selectedMember.FamilyName;
                tbBirthDate.Text = selectedMember.BirthDate.ToString();
                tbAddress.Text = selectedMember.Address;
                tbPostalCode.Text = selectedMember.PostalCode;
                tbCityName.Text = selectedMember.CityName;
                tbEmail.Text = selectedMember.Email;
                tbPhoneNumber.Text = selectedMember.PhoneNumber;
                btAddUpdate.Content = "Update";            
                btDeleteMember.IsEnabled = true;
            }
            else
            {
                selectedMember = null;
                ResetForm();
                btAddUpdate.Content = "Add";
            }
        }

        private void MemberActions_ButtonClick(object sender, RoutedEventArgs e)
        {

        }

        private void AddUpdateMember_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (selectedMember != null)
            {
                try
                {
                    string validationMessage = ValidateForm();
                    if (validationMessage != "")
                    {
                        MessageBox.Show(validationMessage);
                        return;
                    }
                    selectedMember.MemberId = int.Parse(tbMemberId.Text);
                    selectedMember.Name = tbName.Text;
                    selectedMember.FamilyName = tbFamilyName.Text;
                    selectedMember.BirthDate = int.Parse(tbBirthDate.Text);
                    selectedMember.Address = tbAddress.Text;
                    selectedMember.PostalCode = tbPostalCode.Text;
                    selectedMember.CityName = tbCityName.Text;
                    selectedMember.Email = tbEmail.Text;
                    selectedMember.PhoneNumber = tbPhoneNumber.Text;


                    Globals.Db.UpdateMember(selectedMember);
                    MessageBox.Show("Member Is Successfully Updated");
                    ReloadMemberList();
                    ResetForm();

                }
                catch (SqlException ex)
                {
                    MessageBox.Show("The selected member can not be updated " + ex);

                }
            }
            else
            {
                try
                {
                    Member newMember = new Member();
                    string validationMessage = ValidateForm();
                    if (validationMessage != "")
                    {
                        MessageBox.Show(validationMessage);
                        return;
                    }
                    newMember.Name = tbName.Text;
                    newMember.FamilyName = tbFamilyName.Text;
                    newMember.BirthDate = int.Parse(tbBirthDate.Text);
                    newMember.Address = tbAddress.Text;
                    newMember.PostalCode = tbPostalCode.Text;
                    newMember.CityName = tbCityName.Text;
                    newMember.Email = tbEmail.Text;
                    newMember.PhoneNumber = tbPhoneNumber.Text;


                    Globals.Db.AddMember(newMember);
                    MessageBox.Show("Member Is Successfully Added");
                    ReloadMemberList();
                    ResetForm();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("The selected book can not be updated " + ex);

                }
            }
        }

        private void DeleteMember_ButtonClick(object sender, RoutedEventArgs e)
        {
            if (Globals.Db.DeleteMember((lvMembers.SelectedItem as Member).MemberId))
            {
                MessageBox.Show("Member deleted");
                ReloadMemberList();
            }
        }
    }
}
