﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for ReturnLoanDialog.xaml
    /// </summary>
    public partial class ReturnLoanDialog : Window
    {
        List<Loan> LoansList = new List<Loan>();
        public ReturnLoanDialog(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            lvLoans.ItemsSource = LoansList;
            try
            {
                Globals.Db = new Database();
                ReloadLoanList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadLoanList()
        {
            try
            {
                List<Loan> list = Globals.Db.GetAllLoans();
                LoansList.Clear();
                foreach (Loan l in list)
                {
                    LoansList.Add(l);
                }
                lvLoans.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "Book Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void Return_ButtonClick(object sender, RoutedEventArgs e)
        {
            Loan currentLoan = lvLoans.SelectedItem as Loan;
            if(currentLoan == null)
            {
                MessageBox.Show("You should choose a Loan:\n", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            else
            {
                Globals.selecteLoan = currentLoan;
                MessageBoxResult result = MessageBox.Show("Are you sure you want to retrun this loan Id " + Globals.selecteLoan.LoanId + " (Book Id: " + Globals.selecteLoan.BookId+ " Member Id: " + Globals.selecteLoan.MemberId+ "?", "Return Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No);
                if (result == MessageBoxResult.No)
                    return;
                else
                {
                    try
                    {
                        Globals.Db.ReturnABook();
                        MessageBox.Show("Book return successful");
                        ReloadLoanList();
                    }
                    catch (SqlException ex)
                    {
                        MessageBox.Show("Database Error" + Environment.NewLine + ex);
                    }
                }
            }
        }

        private void LvLoans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvLoans.SelectedItem != null)
            {
                btReturn.IsEnabled = true;
            }
            else
            {
                btReturn.IsEnabled = false;
            }
        }
    }
}
