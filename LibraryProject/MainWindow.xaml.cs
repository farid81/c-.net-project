﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        
        List<Loan> LoansList = new List<Loan>();

        public MainWindow()
        {
            InitializeComponent();

        }

        private void ButtonBookSelect_Click(object sender, RoutedEventArgs e)
        {
            BookSelectDialog bookSelectDialog = new BookSelectDialog(this);
            if(bookSelectDialog.ShowDialog() == true)
            {
                return;
            }

            //MainWindow mainWindow = new MainWindow();
            //mainWindow.Visibility = Visibility.Hidden;
            //TODO: Open this window in the center of parent
        }

        private void FileExit_MenuClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void AddUpdateBook_MenuClick(object sender, RoutedEventArgs e)
        {
            AddUpdateBookDialog addUpdateBookDialog = new AddUpdateBookDialog();
            addUpdateBookDialog.ShowDialog();

        }

        private void MmberActions_MenuClick(object sender, RoutedEventArgs e)
        {
            MemberActions memberActions = new MemberActions();
            memberActions.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ReturnLoanDialog returnLoanDialog = new ReturnLoanDialog(this);
            if (returnLoanDialog.ShowDialog() == true)
            {
                return;
            }
        }

        private void BookReport_ButtonClick(object sender, RoutedEventArgs e)
        {
            BookReports booksReport = new BookReports(this);
            booksReport.Show();
        }

        private void MemberReport_ButtonClick(object sender, RoutedEventArgs e)
        {
            MemberReport memberReport = new MemberReport(this);
            memberReport.Show();
        }
    }
}
