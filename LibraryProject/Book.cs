﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibraryProject
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string ISBN { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Subject { get; set; }
        public int YearPublished { get; set; }
        public int NumberOfCopies { get; set; }
        public int NumberOfAvailableCopies { get; set; }
    }
}
