﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for BookReports.xaml
    /// </summary>
    public partial class BookReports : Window
    {
        List<Book> allBooks = new List<Book>();
        public BookReports(Window owner)
        {
            InitializeComponent();
            Owner = owner;
            try
            {
                Globals.Db = new Database();

                ReloadBookComboList();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadBookComboList()
        {

            try
            {
                allBooks = Globals.Db.GetAllBooks();
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
            }
            foreach (Book b in allBooks)
            {
                cmbBookReports.Items.Add(b.Title);
            }
        }

        private void CmbBookReports_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedBookId = allBooks[cmbBookReports.SelectedIndex].BookId;
            List<Loan> currentLoans = Globals.Db.GetAllLoans(selectedBookId);
            lvLoans.ItemsSource = currentLoans;

        }

        private void Print_ButtonClick(object sender, RoutedEventArgs e)
        {
            PrintDialog printDialog = new PrintDialog();

            if (printDialog.ShowDialog() == true)
            {
                printDialog.PrintVisual(lvLoans, "Report");
            }
        }
    }
}
