﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LibraryProject
{
    /// <summary>
    /// Interaction logic for AddUpdateBookDialog.xaml
    /// </summary>
    public partial class AddUpdateBookDialog : Window
    {
        Book selectedBook;
        List<Book> BooksList = new List<Book>();

        public AddUpdateBookDialog()
        {
            InitializeComponent();
            //Owner = owner;
            lvBooks.ItemsSource = BooksList;
            

            try
            {
                Globals.Db = new Database();

                ReloadBookList();


            }
            catch (SqlException ex)
            {
                MessageBox.Show("Fatal error: unable to connect to database\n" + ex.Message,
                    "Library Database", MessageBoxButton.OK, MessageBoxImage.Error);
                Close(); // close the main window, terminate the program
            }
        }

        private void ReloadBookList()
        {
            try
            {
                List<Book> list = Globals.Db.GetAllBooks();
                BooksList.Clear();
                foreach (Book b in list)
                {
                    BooksList.Add(b);
                }
                lvBooks.Items.Refresh();
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Error executing SQL query:\n" + ex.Message,
                    "People Database", MessageBoxButton.OK, MessageBoxImage.Error);
            }catch(NullReferenceException ex)
            {
                MessageBox.Show("There is no book in the database yet" + ex);
            }
        }
        private void ResetForm()
        {
            tbBookId.Text = "";
            tbTitle.Text = "";
            tbISBN.Text = "";
            tbAuthor.Text = "";
            tbDescription.Text = "";
            tbSubject.Text = "";
            tbYearPublished.Text = "";
            tbNumberOfCopies.Text = "";
            tbNumberOfAvailableCopies.Text = "";
            btDeleteBook.IsEnabled = false;
        }

        private string ValidateForm()
        {
            string validationMessage = "";
            if (tbTitle.Text == "")
                validationMessage += "Title can't be empty"+Environment.NewLine;
            if (tbISBN.Text == "")
                validationMessage += "ISBN can't be empty" + Environment.NewLine;
            if (tbAuthor.Text == "")
                validationMessage += "Author can't be empty" + Environment.NewLine;
            if (tbDescription.Text == "")
                validationMessage += "Description can't be empty" + Environment.NewLine;
            if (tbSubject.Text == "")
                validationMessage += "Subject can't be empty" + Environment.NewLine;
            if (tbYearPublished.Text == "")
                validationMessage += "YearPublished can't be empty" + Environment.NewLine;
            if (tbNumberOfCopies.Text == "")
                validationMessage += "NumberOfCopies can't be empty" + Environment.NewLine;
            if (!int.TryParse(tbYearPublished.Text,out int year))
            {
                validationMessage += "YearPublished must be an integer" + Environment.NewLine;
            }
            if (!int.TryParse(tbNumberOfCopies.Text, out int copies))
            {
                validationMessage += "NumberOfCopies must be an integer" + Environment.NewLine;
            }
            return validationMessage;
        }

        private void LvBooks_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LvBooks_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lvBooks.SelectedItem != null)
            {
                selectedBook = lvBooks.SelectedItem as Book;
                
                tbBookId.Text = selectedBook.BookId.ToString();
                tbTitle.Text = selectedBook.Title;
                tbISBN.Text = selectedBook.ISBN;
                tbAuthor.Text = selectedBook.Author;
                tbDescription.Text = selectedBook.Description;
                tbSubject.Text = selectedBook.Subject;
                tbYearPublished.Text = selectedBook.YearPublished.ToString();
                tbNumberOfCopies.Text = selectedBook.NumberOfCopies.ToString();
                tbNumberOfAvailableCopies.Text = selectedBook.NumberOfAvailableCopies.ToString();
                btAddUpdate.Content = "Update";
                tbNumberOfAvailableCopies.IsEnabled = false;
                btDeleteBook.IsEnabled = true;
            }
            else
            {
                tbNumberOfAvailableCopies.IsEnabled = true;
                selectedBook = null;
                ResetForm();
                btAddUpdate.Content = "Add";
            }
        }


        private void AddUpdate_ButtonClick(object sender, RoutedEventArgs e)
        {
            
            if (selectedBook != null)
            {
                try
                {
                    string validationMessage = ValidateForm();
                    if (validationMessage != "")
                    {
                        MessageBox.Show(validationMessage);
                        return;
                    }
                    selectedBook.BookId = int.Parse(tbBookId.Text);
                    selectedBook.Title = tbTitle.Text;
                    selectedBook.ISBN = tbISBN.Text;
                    selectedBook.Author = tbAuthor.Text;
                    selectedBook.Description = tbDescription.Text;
                    selectedBook.Subject = tbSubject.Text;
                    selectedBook.YearPublished = int.Parse(tbYearPublished.Text);
                    selectedBook.NumberOfCopies = int.Parse(tbNumberOfCopies.Text);
                    

                    Globals.Db.UpdateBook(selectedBook);
                    MessageBox.Show("Book Is Successfully Updated");
                    ReloadBookList();
                    ResetForm();

                }catch(SqlException ex)
                {
                    MessageBox.Show("The selected book can not be updated " + ex);

                }
            }
            else
            {
                try
                {
                    Book newBook = new Book();
                    string validationMessage = ValidateForm();
                    if(validationMessage != "")
                    {
                        MessageBox.Show(validationMessage);
                        return;
                    }
                    newBook.Title = tbTitle.Text;
                    newBook.ISBN = tbISBN.Text;
                    newBook.Author = tbAuthor.Text;
                    newBook.Description = tbDescription.Text;
                    newBook.Subject = tbSubject.Text;
                    newBook.YearPublished = int.Parse(tbYearPublished.Text);
                    newBook.NumberOfCopies = int.Parse(tbNumberOfCopies.Text);
                    newBook.NumberOfAvailableCopies = newBook.NumberOfCopies;
                        

                    Globals.Db.AddBook(newBook);
                    MessageBox.Show("Book Is Successfully Added");
                    ReloadBookList();
                    ResetForm();
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("The selected book can not be updated " + ex);

                }
            }
        }

        private void BtDeleteBook_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Globals.Db.DeleteBook((lvBooks.SelectedItem as Book).BookId))
                {
                    MessageBox.Show("Book deleted");
                    ReloadBookList();
                }
            }catch(SqlException ex)
            {
                MessageBox.Show("The book is involved in a loan. You can't delete it" + ex);
            }

        }
    }
}
